package com.vijay.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vijay.exception.MediatorNotFoundException;
import com.vijay.model.Mediator;
import com.vijay.repo.MediatorRepository;
import com.vijay.service.MediatorService;

@Service
public class MediatorServiceImpl implements MediatorService {
    
	
	@Autowired
	private MediatorRepository repo;
	@Override
	public Long createMediator(Mediator mediator) {
		return repo.save(mediator).getId();
	}

	@Override
	public void updateMediator(Mediator mediator) {
		if(mediator.getId()!=null && repo.existsById(mediator.getId()))
		{
			repo.save(mediator);
		}

	}

	@Override
	public Mediator getOneMediator(Long id) {
		Mediator  mediator =repo.findById(id).orElseThrow(
				()-> new MediatorNotFoundException("Mediator  '"+id+"' Not Exists"));
		return mediator;
	}

	@Override
	public List<Mediator> getAllMediators() {
		return repo.findAll();
	}

	@Override
	public void deleteMediator(Long id) {
		Mediator  mediator =repo.findById(id).orElseThrow(
				()-> new MediatorNotFoundException("Mediator  '"+id+"' Not Exists"));
		repo.delete(mediator);;
	}

}
