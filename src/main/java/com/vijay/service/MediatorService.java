package com.vijay.service;

import java.util.List;

import com.vijay.model.Mediator;

public interface MediatorService {
 
	
	
	public  Long createMediator(Mediator mediator);
   
    public void updateMediator(Mediator mediator);
    public Mediator getOneMediator(Long id);
    public List<Mediator> getAllMediators();
    public void deleteMediator(Long id);
}
