package com.vijay.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vijay.model.Mediator;

public interface MediatorRepository extends JpaRepository<Mediator, Long> {

}
