package com.vijay.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vijay.exception.MediatorNotFoundException;
import com.vijay.model.Mediator;
import com.vijay.service.MediatorService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/mediator")
public class MediatorRestController {

	@Autowired
	private MediatorService service;

	//1. create Mediator
	@PostMapping("/create")
	public ResponseEntity<String> createMediator(@RequestBody Mediator mediator) 
	{
		log.info("ENTERED INTO SAVE METHOD");
		ResponseEntity<String> response = null;
		try {
			Long id = service.createMediator(mediator);
			String message="Mediator '"+id+"' CREATED ";
			response = new ResponseEntity<String>(message,HttpStatus.CREATED); 
			log.info("Mediator IS CREATED {}.",id);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		log.info("ABOUT TO LEAVE SAVE METHOD");
		return response;
	}

	//2. fetch all
	@GetMapping("/all")
	public ResponseEntity<List<Mediator>> getAllMediators() 
	{
		ResponseEntity<List<Mediator>> response = null;
		log.info("ENTERED INTO FETCH METHOD");
		try {
			List<Mediator> list = service.getAllMediators();
			response = ResponseEntity.ok(list);
			log.info("FETCH METHOD IS SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		log.info("ABOUT TO LEAVE FETCH ALL METHOD");
		return response;
	}

	//3. fetch one
	@GetMapping("/fetch/{id}")
	public ResponseEntity<Mediator> getOneMediator(@PathVariable Long id)
		
	{
		ResponseEntity<Mediator> response = null;
		log.info("ENTERED INTO FETCH ONE METHOD");
		try {
			Mediator mediator = service.getOneMediator(id);
			
			response = ResponseEntity.ok(mediator);
			log.info("FETCH ONE METHOD IS SUCCESS");
		} catch (MediatorNotFoundException e) {
			//e.printStackTrace();
			log.error(e.getMessage());
			throw e;
		}
		log.info("ABOUT TO LEAVE FETCH ONE METHOD");
		return response;
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<String> deleteMediator(@PathVariable Long id)
		
	{
		ResponseEntity<String> response = null;
		log.info("ENTERED INTO DELETE METHOD");
		try {
		 service.deleteMediator(id);
			response = ResponseEntity.ok("Mediator '"+id+"' Deleted");
			log.info("FETCH ONE METHOD IS SUCCESS");
		} catch (MediatorNotFoundException e) {
			//e.printStackTrace();
			log.error(e.getMessage());
			throw e;
		}
		log.info("LEAVING DELETE METHOD");
		return response;
	}
	@PutMapping("/update")
	public ResponseEntity<String> updateMediator(@RequestBody Mediator mediator) 
	{
		log.info("ENTERED INTO UPDATE METHOD");
		ResponseEntity<String> response = null;
		try {
		    service.updateMediator(mediator);
			response = ResponseEntity.ok("UPDATED WITH ID : " + mediator.getId());
			log.info("Mediator IS CREATED {}.",mediator.getId());
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		log.info("LEAVING UPDATE METHOD");
		return response;
	}

	
}
