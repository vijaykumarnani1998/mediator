package com.vijay.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.vijay.exception.MediatorNotFoundException;


@RestControllerAdvice
public class MediatorCustomExceptionHandler {
	
	@ExceptionHandler (MediatorNotFoundException.class)
    public ResponseEntity<String> handleException(MediatorNotFoundException e)
    {
		return new   ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
    }
}