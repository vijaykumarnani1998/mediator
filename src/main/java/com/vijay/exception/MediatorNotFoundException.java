package com.vijay.exception;

public class MediatorNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1688363487244584261L;
	
	
	public  MediatorNotFoundException()
	{
		super();
		
	}
	public  MediatorNotFoundException(String message)
	{
		super(message);
		
	}

}
