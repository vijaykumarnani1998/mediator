package com.vijay.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="Mediator_Table")
public class Mediator {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="Med_Id_COl")
	private Long id;
	@Column(name="Med_Name_COl")
	private String name;
	@Column(name="Med_Email_COl")
	private String email;
	@Column(name="Med_Address_COl")
	private String address;
	@Column(name="Med_ser_mode_COl")
	private String serviceMode;

	@Column(name="Med_Status_COl")
	private String status;


}
